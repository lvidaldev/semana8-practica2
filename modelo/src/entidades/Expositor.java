package entidades;

public class Expositor extends Persona{

    private String codigo;
    private double sueldo;

    public Expositor(String nombre, String apellidos, String correo, String telefono, String direccion, String codigo, double sueldo) {
        super(nombre, apellidos, correo, telefono, direccion);
        this.codigo = codigo;
        this.sueldo = sueldo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    @Override
    public String toString() {
        return "Expositor{" +
                " Codigo='" + codigo + '\'' +
                " Nombre= " + getNombre() +
                " Sueldo=" + sueldo +
                '}';
    }
}
