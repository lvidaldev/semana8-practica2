package entidades;

import java.security.PublicKey;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import entidades.Asistente;
import entidades.Expositor;
import util.Constants;

public class Evento {

    private String titulo;
    private String duracion;
    private Expositor expositor;
    private String hIngreso;
    private String hSalida;
    private LocalDate fecha;
    private boolean isTemporada;
    private double costo;
    private List<Asistente> asistentes;
    private int capaMax;
    private int capaMin;


    public Evento(String titulo, String duracion, Expositor expositor, String hIngreso, String hSalida, boolean isTemporada, double costo, List<Asistente> asistentes, LocalDate fecha, int capaMax, int capaMin) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.expositor = expositor;
        this.hIngreso = hIngreso;
        this.hSalida = hSalida;
        this.isTemporada = isTemporada;
        this.costo = costo;
        this.asistentes = asistentes;
        this.capaMax = capaMax;
        this.capaMin = capaMin;
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public Expositor getExpositor() {
        return expositor;
    }

    public void setExpositor(Expositor expositor) {
        this.expositor = expositor;
    }

    public String gethIngreso() {
        return hIngreso;
    }

    public void sethIngreso(String hIngreso) {
        this.hIngreso = hIngreso;
    }

    public String gethSalida() {
        return hSalida;
    }

    public void sethSalida(String hSalida) {
        this.hSalida = hSalida;
    }

    public boolean isTemporada() {
        return isTemporada;
    }

    public void setTemporada(boolean temporada) {
        isTemporada = temporada;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public List<Asistente> getAsistentes() {
        return asistentes;
    }

    public void setAsistentes(List<Asistente> asistentes) {
        this.asistentes = asistentes;
    }

    public double evaluarCodigo(String code)
    {
        double resultado;
        switch (code){
            case "P": resultado = Constants.U_PLATINIUM;break;
            case "G": resultado = Constants.U_GOLD;break;
            case "V": resultado = Constants.U_VIP;break;
            default: resultado = Constants.op;
        }
        return resultado ;
    }

    public double costoTotal(String code)
    {
        return (evaluarCodigo(code) * Constants.IGV) + evaluarCodigo(code);
    }

    public double evaluarTemporada(boolean istemp, String code)
    {
        double descuenTotal;
        if(istemp)
            descuenTotal = costoTotal(code) - (costoTotal(code) * Constants.TEMPORADA_ALTA);
        else
            descuenTotal = costoTotal(code) - (costoTotal(code) * Constants.TEMPORADA_BAJA);

        return descuenTotal;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public int getCapaMax() {
        return capaMax;
    }

    public void setCapaMax(int capaMax) {
        this.capaMax = capaMax;
    }

    public int getCapaMin() {
        return capaMin;
    }

    public void setCapaMin(int capaMin) {
        this.capaMin = capaMin;
    }

    @Override
    public String toString() {
        return "Evento{" +
                "titulo='" + titulo + '\'' +
                ", duracion='" + duracion + '\'' +
                ", expositor=" + expositor +
                ", hIngreso='" + hIngreso + '\'' +
                ", hSalida='" + hSalida + '\'' +
                ", fecha=" + fecha +
                ", isTemporada=" + isTemporada +
                ", costo=" + costo +
                ", asistentes=" + asistentes +
                ", capaMax=" + capaMax +
                ", capaMin=" + capaMin +
                '}';
    }
}
