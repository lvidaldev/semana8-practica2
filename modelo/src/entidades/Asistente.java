package entidades;

public class Asistente extends Persona{

    private String codigo;

    public Asistente(String nombre, String apellidos, String correo, String telefono, String direccion, String codigo) {
        super(nombre, apellidos, correo, telefono, direccion);
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Asistente{" +
                " Nombre= " + getNombre()+
                " codigo= " + codigo +
                '}';
    }

}
