package repository;

import entidades.Evento;

import java.util.List;
import java.util.Optional;

public interface EventoRepository<T> {

    Optional<Evento> buscarPorNombre();
    List<Evento> listarOrdEventos();
    List<Evento> listarEventos(String fechaInic, String fechaFinal);
    List<Evento> eventosCostosPromD();
    List<Evento> eventosCostosPromE();
}
