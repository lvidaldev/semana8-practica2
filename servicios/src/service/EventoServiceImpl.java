package service;

import entidades.Asistente;
import entidades.Evento;
import entidades.Expositor;
import repository.EventoRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EventoServiceImpl implements EventoRepository {

    private List<Evento> nuevoEvento;





    public EventoServiceImpl() {

        //assistentes
        Asistente asistente1 = new Asistente("Luis","Vidal","lvidal","8541545","Av asdasd", "32323");
        List<Asistente> asistentes = new ArrayList<>();
        asistentes.add(asistente1);

        //expositores
        Expositor expositor1 = new Expositor("EXPOSITOR1","EXPO","EXP@","5456454","AV 65654", "654654",5000);


        this.nuevoEvento = new ArrayList<>();
        nuevoEvento.add(new Evento("Titulo","1",expositor1, "1:00", "2:00",true,200.00,asistentes, LocalDate.of(2017, 1, 13),20,5));
    }

    @Override
    public Optional<Evento> buscarPorNombre() {
        return Optional.empty();
    }

    @Override
    public List<Evento> listarOrdEventos() {
        return nuevoEvento.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<Evento> listarEventos(String fechaInic, String fechaFinal) {
        return null;
    }

    @Override
    public List<Evento> eventosCostosPromD() {
        return null;
    }

    @Override
    public List<Evento> eventosCostosPromE() {
        return null;
    }
}
