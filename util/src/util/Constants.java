package util;

public class Constants {

    public static final double U_PLATINIUM = 250.50;
    public static final double U_GOLD = 150.50;
    public static final double U_VIP = 95.00;
    public static double op = U_PLATINIUM;

    public static final double IGV = 0.18;

    public static final double TEMPORADA_ALTA = 0.05;
    public static final double TEMPORADA_BAJA = 0.10;


}
